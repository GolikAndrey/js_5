function Student(name, lastName) {
    this.firstName = name;
    this.lastName = lastName;
    this.getFullName = function () {
        return `${this.firstName} ${this.lastName}`;
    }
};

const student1 = new Student('Vasya', 'Ivanov');

console.log(student1.getFullName());
